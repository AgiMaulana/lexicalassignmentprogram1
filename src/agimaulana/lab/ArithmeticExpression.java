package agimaulana.lab;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ArithmeticExpression {
    private int STATE_I = 0;
    private int STATE_P = 1;
    private int STATE_Q = 2;
    private int STATE_F = 3;

    private final String OUTPUT_NUM = "num";
    private final String OUTPUT_OPT = "opt";
    private final String OUTPUT_ERROR = "error";
    private final String OUTPUT_BRACKET_OPEN = "kurbuka";
    private final String OUTPUT_BRACKET_CLOSE = "kurtutup";

    private final String HASH = "#";
    private final String S = "S";
    private final String OPR = "opr";
    private final String SoprS = "SoprS";
    private final String BRACKET_S = "(S)";
    private final String BRACKET_OPEN = "(";
    private final String BRACKET_CLOSE = ")";
    private final String NUM = "num";

    private List<String> lexicAnalysisOutput;
    private List<String> lexicAnalysisOutputTemp;
    private Stack<String> stack = new Stack<>();
    private int k = 0;

    public ArithmeticExpression(List<String> lexicAnalysisOutput) {
        this.lexicAnalysisOutput = lexicAnalysisOutput;
        this.lexicAnalysisOutputTemp = new ArrayList<>();
        lexicAnalysisOutputTemp.addAll(lexicAnalysisOutput);
    }

    public void exec(){
        int mState = STATE_I;
        stack.push(HASH);
        mState = STATE_P;
        stack.push(S);
        boolean pop = false;
        String symbol;
        while (!stack.peek().equalsIgnoreCase(HASH)){
            symbol = getCurrentSymbol();
            if(symbol.equalsIgnoreCase(OUTPUT_ERROR))
                break;
            switch (stack.peek()){
                case S:
                    pop = pop(S);
                    if(isNum(symbol)){
                        stack.push(NUM);
                    }else if (isBracketOpen((symbol))){
                        stack.push(BRACKET_OPEN);
                    }else{
                        pop = false;
                    }
                    break;
                case OPR:
                    pop = pop(OPR);
                    k++;
                    if(isNum(getCurrentSymbol()))
                        stack.push(NUM);
                    break;
                case NUM:
                    pop = pop(NUM);
                    if(k < lexicAnalysisOutput.size()-1)
                        k++;
                    if(isOpt(getCurrentSymbol()))
                        stack.push(OPR);
                    break;
                case BRACKET_OPEN:
                    pop = pop(BRACKET_OPEN);
                    k++;
                    break;
            }
            symbol = getCurrentSymbol();
            if(!pop || symbol.equalsIgnoreCase(OUTPUT_ERROR))
                break;
            System.out.println(symbol);
            System.out.println(stack);
        }
        if(pop(HASH) && !getCurrentSymbol().equalsIgnoreCase(OUTPUT_ERROR)){
            mState = STATE_F;
            System.out.println(stack);
            System.out.println("Valid");
        }else{
            System.out.println(stack);
            System.out.println("Invalid");
        }
    }

    private boolean pop(String symbol){
        if(stack.peek().equalsIgnoreCase(symbol)){
            stack.pop();
            return true;
        }
        return false;
    }

    private String getCurrentSymbol(){
        //        k++;
        return lexicAnalysisOutputTemp.get(k);
    }

    private boolean isEos(){
        return k == lexicAnalysisOutput.size();
    }

    private boolean isNum(String ouput){
        return ouput.equalsIgnoreCase(OUTPUT_NUM);
    }

    private boolean isOpt(String ouput){
        return ouput.equalsIgnoreCase(OUTPUT_OPT);
    }

    private boolean isError(String ouput){
        return ouput.equalsIgnoreCase(OUTPUT_ERROR);
    }

    private boolean isBracketOpen(String ouput){
        return ouput.equalsIgnoreCase(OUTPUT_BRACKET_OPEN);
    }

    private boolean isBracketClose(String ouput){
        return ouput.equalsIgnoreCase(OUTPUT_BRACKET_CLOSE);
    }
}
