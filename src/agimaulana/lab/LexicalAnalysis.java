package agimaulana.lab;

import java.util.*;

public class LexicalAnalysis {
    private final String OUTPUT_NUM = "num";
    private final String OUTPUT_OPT = "opt";
    private final String OUTPUT_ERROR = "error";
    private final String OUTPUT_BRACKET_OPEN = "kurbuka";
    private final String OUTPUT_BRACKET_CLOSE = "kurtutup";
    private final char SPACE = ' ';
    private final char COMMA = ',';
    private final char PLUS_SIGN = '+';
    private final char MIN_SIGN = '-';
    private final char BRACKET_OPEN = '(';
    private final char BRACKET_CLOSE = ')';
    private final char E = 'E';
    private final int STATE_INITIAL = 0;
    private final int STATE_OPERATOR = 1;
    private final int STATE_PLUS_MINUS_VALID = 2;
    private final int STATE_PLUS_MINUS_NOT_ACCEPTED = 7;
    private final int STATE_NUMBER = 3;
    private final int STATE_NUMBER_2 = 9;
    private final int STATE_NUMBER_3 = 10;
    private final int STATE_COMMA = 4;
    private final int STATE_ERROR = 5;
    private final int STATE_E = 6;
    private final int STATE_BRACKET_OPEN = 9;
    private final int STATE_BRACKET_CLOSE = 8;

    private final char[] NUMBERS = {'0','1','2','3','4','5','6','7','8','9'};
    private final char[] OPERATORS = {'x',':'};

    public static void main(String[] args) {
	// write your code here
        LexicalAnalysis lexicalAnalysis = new LexicalAnalysis();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input (String)            : ");
        String input = scanner.nextLine() + " ";
        List<String> output = lexicalAnalysis.analytic(input.toCharArray());
        System.out.println("Output (Token Lexic)    : " + output);
        new ArithmeticExpression(output).exec();
    }

    public List<String> analytic(char[] inputs){
        List<String> output = new ArrayList<>();
        int mState = STATE_INITIAL;
        for (char c : inputs){
            if(!isSpace(c)){
                if(mState == STATE_INITIAL){
                    if(c == BRACKET_OPEN)
                        output.add(OUTPUT_BRACKET_OPEN);
                    else if(c == BRACKET_CLOSE)
                        output.add(OUTPUT_BRACKET_CLOSE);
                    else if(isOperator(c))
                        output.add(OUTPUT_OPT);
                    else if(c == PLUS_SIGN || c == MIN_SIGN)
                        mState = STATE_PLUS_MINUS_VALID;
                    else if(isNumber(c))
                        mState = STATE_NUMBER;
                    else if(!isBracket(c) && !isNumber(c) && c != PLUS_SIGN && c != MIN_SIGN)
                        mState = STATE_ERROR;
                }else if(!isBracket(c)){
                    if(mState == STATE_PLUS_MINUS_VALID){
                        if(isNumber(c))
                            mState = STATE_NUMBER;
                        else
                            mState = STATE_ERROR;
                    }else if(mState == STATE_NUMBER){
                        if(isNumber(c))
                            mState = STATE_NUMBER;
                        else if(isComma(c))
                            mState = STATE_COMMA;
                        else if(c == E)
                            mState = STATE_E;
                        else
                            mState = STATE_ERROR;
                    }else if(mState == STATE_COMMA)
                        if(isNumber(c))
                            mState = STATE_NUMBER_2;
                        else
                            mState = STATE_ERROR;
                    else if(mState == STATE_E){
                        if(isNumber(c))
                            mState = STATE_NUMBER_3;
                        else if(c == MIN_SIGN || c == PLUS_SIGN)
                            mState = STATE_PLUS_MINUS_NOT_ACCEPTED;
                        else
                            mState = STATE_ERROR;
                    }else if(mState == STATE_PLUS_MINUS_NOT_ACCEPTED){
                        if(isNumber(c))
                            mState = STATE_NUMBER_3;
                        else
                            mState = STATE_ERROR;
                    }else if(mState == STATE_NUMBER_2){
                        if(isNumber(c))
                            mState = STATE_NUMBER_2;
                        else
                            mState = STATE_ERROR;
                    }else if(mState == STATE_NUMBER_3){
                        if(isNumber(c))
                            mState = STATE_NUMBER_3;
                        else
                            mState = STATE_ERROR;
                    }
                }
            }

            if(!isNumber(c) && !isOperator(c) && !isComma(c) && !isBracket(c) && !isSpace(c) && c != E
                    && c != MIN_SIGN && c != PLUS_SIGN)
                mState = STATE_ERROR;

            if(mState != STATE_INITIAL && (isSpace(c) || isBracket(c))){
                if(mState == STATE_ERROR || mState == STATE_COMMA || mState == STATE_E || mState == STATE_PLUS_MINUS_NOT_ACCEPTED){
                    output.add(OUTPUT_ERROR);
                    mState = STATE_INITIAL;
                    break;
                }

                switch (mState){
                    case STATE_OPERATOR:
                    case STATE_PLUS_MINUS_VALID:
                        output.add(OUTPUT_OPT);
                        mState = STATE_INITIAL;
                        break;
                    case STATE_NUMBER:
                    case STATE_NUMBER_2:
                    case STATE_NUMBER_3:
                        output.add(OUTPUT_NUM);
                        mState = STATE_INITIAL;
                        break;
                }

                if(c == BRACKET_OPEN){
                    output.add(OUTPUT_BRACKET_OPEN);
                    mState = STATE_INITIAL;
                }else if(c == BRACKET_CLOSE){
                    output.add(OUTPUT_BRACKET_CLOSE);
                    mState = STATE_INITIAL;
                }
            }

        }
        return output;
    }

    private boolean isNumber(char c){
        for (char aChar : NUMBERS)
            if(c == aChar)
                return true;
        return false;
    }

    private boolean isOperator(char c){
        for (char aChar : OPERATORS)
            if(c == aChar)
                return true;
        return false;
    }

    private boolean isBracket(char c){
        return c == '(' || c == ')';
    }

    private boolean isSpace(char c){
        return c == SPACE;
    }

    private boolean isComma(char c){
        return c == COMMA;
    }

}
