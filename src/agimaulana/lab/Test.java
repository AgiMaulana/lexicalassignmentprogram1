package agimaulana.lab;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args){
        List<String> inputs = new ArrayList<>(){{
            /*add("+6");
            add("-7");
            add("+6E");
            add("5000");
            add("5000,032");
            add("+7E+3");
            add("+4E-3");
            add("+4,3");
            add("+7,8");
            add("+");
            add("-");
            add("x");
            add(":");
            add("(");
            add(")");
            add(",");*/
            add("3 + 2");
//            add("%");
//            add("2 + 3 x 10");
//            add("-3 x (7 + 10)");
//            add("3 x 7 %(2 + 3)");
//            add("3 x 7 %)2 + 3)");
//            add("2+3x10");
//            add("2 + 3x 10");
            /*add("+6E+3");
            add("6,6");
            add("6,6,6");
            add("6E6E6");*/
//            add("4 + 5");
        }};

        LexicalAnalysis lexicalAnalysis = new LexicalAnalysis();
        List<String> output = new ArrayList<>();
        for (String s : inputs){
            output.clear();
            System.out.println("Input (String)            : " + s);
            output.addAll(lexicalAnalysis.analytic(s.concat(" ").toCharArray()));
            System.out.println("Output (Token Lexic)      :" + output);
            new ArithmeticExpression(output).exec();
            System.out.println();
        }
    }
}
